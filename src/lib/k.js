function idcardAspUrl () { return 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/' }
function idcardUrl () {
  const url = new URL(window.location.href)
  return url.toString().split('#')[0]
} // generic method to convert to integer or 0 from text
function i (text) { return parseInt(text) || 0 } // generic method to convert to integer or 0 from text
function si (obj, keys) {
  let res = 0
  keys.forEach(e => {
    res += i(obj[e])
  })
  return res
  // i(obj[keys[0]]) + i(obj[keys[1]])
} // generic method to sum integer or 0 from text
// function maxRoundedScale(maxValue) is to get tens\hundreds\thousands rounded values from value
// in order to set rounded scales on the charts
function maxRoundedScale (maxValue) {
  let str = Math.ceil(maxValue).toString()
  let firstDigit = parseInt(str.substring(0, 1))
  const order = str.length
  let res = Math.pow(10, order - 1) * (firstDigit + 1)
  if (maxValue > 0 && maxValue < 1) {
    str = maxValue.toString()
    firstDigit = parseInt(str.substring(2, 3))
    res = 0.1 * (firstDigit + 1)
  }
  return res
}
function maxValueInField (dataset, field) {
  let max = dataset[0][field]
  if (dataset) {
    max = dataset.reduce(
      (max, item) => {
        const current = parseFloat(item[field])
        if (current > max) max = current
        return max
      },
      max)
  }
  return max
}
function minValueInField (dataset, field) {
  let min = dataset[0][field]
  if (dataset) {
    min = dataset.reduce(
      (min, item) => {
        const current = parseFloat(item[field])
        if (current < min) min = current
        return min
      },
      min)
  }
  return min
}
export default {
  i,
  si,
  maxRoundedScale,
  maxValueInField,
  minValueInField,
  idcardAspUrl,
  idcardUrl
}
