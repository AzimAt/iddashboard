// router/index.js

// Router
import HomePage from '../views/HomePage.vue'
import HomeType from '../views/HomeType.vue'
import IntermediatePage from '../views/IntermediatePage.vue'

import ProfDrillholes from '../views/prof/ProfDrillholes.vue'
import Drillholes from '../views/id/Drillholes.vue'

import IDCard from '../views/id/IDCard.vue'
import DHData from '../views/dh/DHData.vue'
import DHDetails from '../views/dh/DHDetails.vue'
import AdminRights from '../views/ad/AdminRights.vue'

import RProdRappSomm from '../views/rp/RProdRappSomm.vue'
import RImpValRadioEcology from '../views/rp/RImpValRadioEcology.vue'

import RImpSamplesAccompList from '../views/rp/RImpSamplesAccompList.vue'
import RImpSamplesStandard from '../views/rp/RImpSamplesStandard.vue'
import RImpSamplesDespatch from '../views/rp/RImpSamplesDespatch.vue'
import RImpSamplesXRF from '../views/rp/RImpSamplesXRF.vue'

import QCResultsByDeposit from '../views/depo/QCResultsByDeposit.vue'
import DepoDrillholes from '../views/depo/DepoDrillholes.vue'
import DepoSamples from '../views/depo/DepoSamples.vue'

import DHSummary1 from '../views/dh/DHSummary1.vue'
import DHSummary2 from '../views/dh/DHSummary2.vue'
import DHSummary3 from '../views/dh/DHSummary3.vue'

import BHTO25Annex1 from '../views/bh/TO25Annex1.vue'
import BHDesign from '../views/bh/Design.vue'

import RCGISSelect from '../views/rc/GISSelect.vue'
import RCGT from '../views/rc/GT.vue'
import RCHSGT from '../views/rc/HSGT.vue'
import RCGRM from '../views/rc/GRM.vue'
import TBOsidemQC from '../views/id/TBOsidemQC.vue'

import DHSummary4 from '../views/dh/DHSummary4.vue'
import DH3Dhistories from '../views/id/DH3Dhistories.vue'

import DHXRF from '../views/dh/DHXRF.vue'
import DHXRFChart from '../views/dh/DHXRFChart.vue'

import DHSamples from '../views/dh/DHSamples.vue'
import DHSamplesGra from '../views/dh/DHSamplesGra.vue'
import DHSamplesCo  from '../views/dh/DHSamplesCo.vue'
import DHMIXChart   from '../views/dh/DHMIXChart.vue'

import AccessDeniedPage from '../views/AccessDeniedPage.vue'
import DemDashboard   from '../views/bh/DemDashboard.vue'

console.log('h1. bef read routes')
const routes = [
  { path: '/', component: HomePage },
  { path: '/b/', component: HomeType, meta: { header_title: 'Tech.block (DEM) info' } },
  { path: '/b/:p1', component: IntermediatePage, meta: { header_title: 'Tech.block (DEM)' },
    children: [
      { path: '/b/:p1/id/idcard', name: 'IDCard', component: IDCard, meta: {  menu_title: 'ID Card', target_blank: false, tooltip: 'ID Card' } },
      { path: '/b/:p1/rc/gisselect', name: 'RCGISSelect', component: RCGISSelect, meta: {  menu_title: 'GIS selection', tooltip: 'GIS selection' } },
      { path: '/b/:p1/rc/gt', name: 'RCGT', component: RCGT, meta: {  menu_title: 'GT inventory from AtomGeo', tooltip: '' } },
      { path: '/b/:p1/rc/et', name: 'RCGRM', component: RCGRM, meta: {  menu_title: 'Effective thickness', tooltip: '' } }, 
      { path: '/b/:p1/rc/hsgt', name: 'RCHSGT', component: RCHSGT, meta: {  menu_title: 'GT historical from acQuire', tooltip: '' } },
      { path: '/b/:p1/id/drillholes', name: 'Drillholes', component: Drillholes, meta: {  menu_title: 'List of drillholes', tooltip: '' } },
      { path: '/b/:p1/dh/summary1', name: 'DHSummary1', component: DHSummary1, meta: {  menu_title: 'Summary by status/purpose', tooltip: 'Summary by status/purpose' }  },
      { path: '/b/:p1/dh/logging', name: 'DHSummary2', component: DHSummary2, meta: {  menu_title: 'Summary by logging', tooltip: '' } },
      { path: '/b/:p1/dh/giklets', name: 'DHSummary3', component: DHSummary3, meta: {  menu_title: 'Summary by giklet', tooltip: '' } },
      { path: '/b/:p1/bh/design', name: 'BHDesign', component: BHDesign, meta: {  menu_title: 'Shape editor tracking', tooltip: '' } },
      { path: '/b/:p1/bh/to25annex1', name: 'BHTO25Annex1', component: BHTO25Annex1, meta: {  menu_title: 'TO25 annex 1', tooltip: '' } },
      { path: '/b/:p1/qc/osidem1', name: 'TBOsidemQC', component: TBOsidemQC, meta: {  menu_title: 'Osidem QC for Modeling', tooltip: '' } },
      { path: '/b/:p1/demdash', component: DemDashboard, meta: {  menu_title: 'DEM dashboard', tooltip: '' } },
      { path: '/b/:p1/*', redirect: '/b/:p1/id/idcard' }
    ]
  },
  { path: '/depo/', component: HomeType, meta: { header_title: 'Deposit info' } },
  { path: '/depo/:p1', component: IntermediatePage, meta: { header_title: 'Deposit' }, 
    children: [
      { path: '/depo/:p1/id/idcard', name: 'DepoIDCard', component: IDCard, meta: {  menu_title: 'ID Card', tooltip: '' } },
      { path: '/depo/:p1/id/deposamples', name: 'DepoSamples', component: DepoSamples, meta: {  menu_title: 'Reserves estimation', tooltip: '' } },
      { path: '/depo/:p1/id/drillholes', component: DepoDrillholes, meta: {  menu_title: 'List of drillholes', tooltip: '' } },
      { path: '/depo/:p1/qc/qcresults', component: QCResultsByDeposit, meta: {  menu_title: 'List of QC results', tooltip: '' } },
      { path: '/depo/:p1/rc/gt', component: RCGT, meta: {  menu_title: 'GT inventory from AtomGeo', tooltip: '' } },
      { path: '/depo/:p1/rc/hsgt', component: RCHSGT, meta: {  menu_title: 'GT historical from acQuire', tooltip: '' } },
      { path: '/depo/:p1/*', redirect: '/depo/:p1/id/idcard' }
    ]
  },
  { path: '/w/', component: HomeType, meta: { header_title: 'Wells information'} },
  { path: '/w/:p1', component: IntermediatePage, meta: { header_title: 'Well' },
    children: [
      { path: '/w/:p1/data', name: 'DHData', component: DHData, meta: {  menu_title: '', tooltip: '' } }, /* not used */
      { path: '/w/:p1/details', name: 'DHDetails', component: DHDetails, meta: {  menu_title: 'Details', tooltip: '' } },
      { path: '/w/:p1/xrf', name: 'DHXRF', component: DHXRF, meta: {  menu_title: 'XRF picking', tooltip: '' } }, 
      { path: '/w/:p1/xrfchart', name: 'DHXRFChart', component: DHXRFChart, meta: {  menu_title: 'XRF chart', tooltip: '' } },
      { path: '/w/:p1/samples', name: 'DHSAMPLES', component: DHSamples, meta: {  menu_title: '', tooltip: '' } },
      { path: '/w/:p1/samplesgra', name: 'DHSAMPLESGRA', component: DHSamplesGra, meta: {  menu_title: '', tooltip: '' } },
      { path: '/w/:p1/samplesco', name: 'DHSAMPLESCO', component: DHSamplesCo, meta: {  menu_title: '', tooltip: '' } },
      { path: '/w/:p1/mixchart', name: 'DHMIXChart', component: DHMIXChart, meta: {  menu_title: '', tooltip: '' } },
      { path: '/w/:p1/*', redirect: '/w/:p1/details' }
    ]
  },
  { path: '/g3d/', component: HomeType, meta: { header_title: '3D Model info' }  },
  { path: '/g3d/:p1', component: IntermediatePage, meta: { header_title: '3D Model' },
    children: [
      { path: '/g3d/:p1/id/drillholes',  component: Drillholes, meta: {  menu_title: 'List of drillholes', tooltip: '' } },
      { path: '/g3d/:p1/dh/summary1', component: DHSummary1, meta: {  menu_title: 'Summary by status/purpose', tooltip: '' } },
      { path: '/g3d/:p1/dh/logging', component: DHSummary2, meta: {  menu_title: 'Summary by logging', tooltip: '' } },
      { path: '/g3d/:p1/dh/giklets', component: DHSummary3, meta: {  menu_title: 'Summary by giklet', tooltip: '' } },
      { path: '/g3d/:p1/dh/3dmodel', component: DHSummary4, meta: {  menu_title: 'Summary for 3D model', tooltip: '' } },
      { path: '/g3d/:p1/id/dh3dhistories', component: DH3Dhistories, meta: {  menu_title: 'History of 3d model list', tooltip: '' } },
      { path: '/g3d/:p1/rc/gisselect', component: RCGISSelect, meta: {  menu_title: 'GIS selection', tooltip: '' } },
      { path: '/g3d/:p1/rc/gt', component: RCGT, meta: {  menu_title: 'GT inventory from AtomGeo', tooltip: '' } },
      { path: '/g3d/:p1/rc/et', component: RCGRM, meta: {  menu_title: 'Effective thickness', tooltip: '' } }, 
      { path: '/g3d/:p1/rc/hsgt', component: RCHSGT, meta: {  menu_title: 'GT historical from acQuire', tooltip: '' } }, 
      { path: '/g3d/:p1/*', redirect: '/g3d/:p1/id/drillholes' }
    ]
  },
  { path: '/gtb/', component: HomeType, meta: { header_title: 'Tech.block Estimation info' } },
  { path: '/gtb/:p1', component: IntermediatePage, meta: { header_title: 'Tech.block Estimation' },
    children: [
      { path: '/gtb/:p1/id/drillholes', component: Drillholes, meta: {  menu_title: 'List of drillholes', tooltip: '' }  },
      { path: '/gtb/:p1/rc/gisselect', component: RCGISSelect, meta: {  menu_title: 'GIS selection', tooltip: '' }  },
      { path: '/gtb/:p1/dh/summary1', component: DHSummary1, meta: {  menu_title: 'Summary by status/purpose', tooltip: '' }  },
      { path: '/gtb/:p1/dh/logging', component: DHSummary2, meta: {  menu_title: 'Summary by logging', tooltip: '' }  },
      { path: '/gtb/:p1/dh/giklets', component: DHSummary3, meta: {  menu_title: 'Summary by giklet', tooltip: '' }  },
      { path: '/gtb/:p1/bh/design', component: BHDesign, meta: {  menu_title: 'Estimations and LTMP Designs', tooltip: '' }  },
      { path: '/gtb/:p1/rc/gt', component: RCGT, meta: {  menu_title: 'GT inventory from AtomGeo', tooltip: '' }  },
      { path: '/gtb/:p1/rc/et', component: RCGRM, meta: {  menu_title: 'Effective thickness', tooltip: '' }  }, 
      { path: '/gtb/:p1/rc/hsgt', component: RCHSGT, meta: {  menu_title: 'GT historical from acQuire', tooltip: '' }  },
      { path: '/gtb/:p1/*', redirect: '/gtb/:p1/id/drillholes' }
    ]
  },
  { path: '/prof/', component: HomeType, meta: { header_title: 'Profile info' } },
  { path: '/prof/:p1', component: IntermediatePage, meta: { header_title: 'Profile' },
    children: [
      { path: '/prof/:p1/id/drillholes', component: ProfDrillholes, meta: {  menu_title: 'List of drillholes', tooltip: '' }  },
      { path: '/prof/:p1/*', redirect: '/prof/:p1/id/drillholes' }
    ]
  },
  { path: '/r/', component: HomeType, meta: { header_title: 'Report info' } },
  { path: '/r/:p1', component: IntermediatePage, meta: { header_title: 'Report' },
    children: [
      { path: '/r/:p1/rappsomm', name: 'RPRapSom', component: RProdRappSomm, meta: {  menu_title: 'QC OSIDEM DAILY RAPPORT SOMMAIRE', tooltip: '' }  },
      { path: '/r/:p1/radeco', name: 'RIVRadioEcology', component: RImpValRadioEcology, meta: {  menu_title: 'RADIOECOLOGY IMPORT VALIDATION', tooltip: '' }  },
      { path: '/r/:p1/*', redirect: '/r/:p1/rappsomm' }
    ]
  },
  { path: '/s/', component: HomeType, meta: { header_title: 'Sampling information' } },
  { path: '/s/:p1', component: IntermediatePage, meta: { header_title: 'Sampling' },
    children: [
      { path: '/s/:p1/al', name: 'RImpSamplesAccompList', component: RImpSamplesAccompList, meta: {  menu_title: 'IMPORT ACCOMPANYING LIST', tooltip: 'Загрузить cопроводительный лиcт опробованиѝ керна cкважины'} },
      { path: '/s/:p1/ss', name: 'RImpSamplesStandard', component: RImpSamplesStandard, meta: {  menu_title: 'IMPORT STANDARD SAMPLES', tooltip: 'Загрузить cтандартные пробы' } },
      { path: '/s/:p1/sd', name: 'RImpSamplesDespatch', component: RImpSamplesDespatch, meta: {  menu_title: 'IMPORT DESPATCH SAMPLES', tooltip: 'Загрузить cводную опиcь проб (U-Ra,Gr,CO)' } },
      { path: '/s/:p1/sx', name: 'RImpSamplesXRF', component: RImpSamplesXRF, meta: {  menu_title: 'IMPORT XRF SAMPLES', tooltip: 'Загрузить XRF-пробы' } },
      { path: '/s/:p1/*', redirect: '/s/:p1/al' }
    ]
  },
  { path: '/ad/', component: HomeType, meta: { header_title: 'Admin unit info' } },
  { path: '/ad/:p1', component: IntermediatePage, meta: { header_title: 'Admin unit' },
    children: [
      { path: '/ad/:p1/rights', name: 'AdAdminRights', component: AdminRights, meta: {  menu_title: 'User access administrating', tooltip: '' }  },
      { path: '/ad/:p1/*', redirect: '/ad/ad2/rights' }
    ]
  }
]
// console.log('h2. after read routes. routes = ', routes)

// Apply user rights to routes. For inaccessible routes we reset component to AccessDeniedPage.
const urlR = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=PORTAL_ALLOWEDROUTES_BY_USERNAME&p1=NULL'
const obj = await (await fetch(urlR, { credentials: 'include' })).json()
const rightList = obj.content
console.log('T01. rightList = ', rightList)
routes.forEach(vuerouteel => {
  if (vuerouteel.children) {
    vuerouteel.children.forEach(el2 => {
      const arr = rightList.filter((el) => { return el.routepath === el2.path })
      // console.log('el2.path = ', el2.path, ', arr = ', arr)
      // const arr = rightList.filter(function (el: { routepath: string }) { return el.routepath === el2.path })
      // console.log('el2.component.name = ', el2.component.name)
      if (el2.component && el2.component.name==='DemDashboard') { console.log('el2.component.name = ', el2.component.name); return; }
      if (arr.length === 0) {
        el2.component = AccessDeniedPage
      }
    })
  }
})


export default routes;