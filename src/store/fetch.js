function getConfig (url) {
  const config = { credentials: 'include' }
  if (url.match(/\/piwebapi\//gi)) {
    config.headers = {
      'Content-Type': 'application/json; charset=utf-8',
      Accept: 'application/json; charset=utf-8'
    }
  }
  return config
}

function fget (url) {
  return window.fetch(url, {
    method: 'GET',
    ...getConfig(url)
  }).then(async res => {
    const data = await res.json()
    if (res.ok) {
      if (data.result !== 1) {
        console.log('Download error: ' + url + ' : ' + data.result.error)
      }
      return data
    } else {
      console.log('There was an error in fetch (Promise.reject): ' + data.error)
      return Promise.reject(data)
    }
  }).catch(error => {
    console.log('There was an error (catch): ' + error)
  })
}

export default {
  fget
}
