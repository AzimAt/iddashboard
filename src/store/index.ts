import Vue from 'vue'
import Vuex from 'vuex'

import ui from './modules/ui/ui'
import ui2 from './modules/ui/ui2'
import ui3 from './modules/ui/ui3'

import tblocks from './modules/blocks/tblocks'
import tblocksgeos from './modules/blocks/tblocksgeos'
import geos3d from './modules/ui/geos3d'
import profiles from './modules/profile/profiles'
import deposits from './modules/deposit/deposits'
import qcresultsbydeposit from './modules/deposit/qcresultsbydeposit'
import depositsamples from './modules/deposit/depositsamples'

import dhs from './modules/ui/dhs'
import sde from './modules/blocks/sde'
import sdecells from './modules/blocks/sdecells'
import depositparam from './modules/blocks/depositparam'
// added by AN 18.12.2023 for osidem QC feature https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=BLOCK_INJECTING_PRODUCERS&p1=%
import injectingproducers from './modules/blocks/injectingproducers'

import bhistory from './modules/bh/bhistory'
import to25annex1 from './modules/bh/to25annex1'

import drillholes from './modules/wells/drillholes'
import dhsummary1 from './modules/wells/dhsummary1'
import dhsummary2 from './modules/wells/dhsummary2'
import dhsummary3 from './modules/wells/dhsummary3'
import dh3dhistory from './modules/wells/dh3dhistory'

import wells from './modules/wells/wells'
import wellspiacq from './modules/wells/wellspiacq'

// added by YS on 2021-12-24 for https://dev.azure.com/orano/DevOps%20BU%20Mines/_backlogs/backlog/idcard/Features/?workitem=29071
import wellsgtmap from './modules/wells/wellsgtmap'

import wellsgt from './modules/wells/wellsgt'
import wellsgrm from './modules/wells/wellsgrm'
import wellshsgt from './modules/wells/wellshsgt'
import holedetails from './modules/wells/holedetails'
// added by YS on 2022-01-26 for showing XRF data by 1 well
import holexrf from './modules/wells/holexrf'
// added by Azim on 2022-12-27 for showing Samples data by 1 well
import holesamples from './modules/wells/holesamples'
import holesamplesgra from './modules/wells/holesamplesgra'
import holesamplesco from './modules/wells/holesamplesco'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    debugmode: false,
    showAppDrawer: null,
    devMode: false
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    ui,
    ui2,
    ui3,
    tblocks,
    tblocksgeos,
    geos3d,
    profiles,
    deposits,
    qcresultsbydeposit,
    depositsamples,
    dhs,
    sde,
    sdecells,
    depositparam,
    injectingproducers,
    bhistory,
    to25annex1,
    drillholes,
    dhsummary1,
    dhsummary2,
    dhsummary3,
    dh3dhistory,
    wells,
    wellspiacq,
    wellsgtmap,
    wellsgt,
    wellsgrm,
    wellshsgt,
    holedetails,
    holexrf,
    holesamples,
    holesamplesgra,
    holesamplesco
  }
})
