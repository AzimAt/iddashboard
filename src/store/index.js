// this file not used. Azim. 2024-01-24

// import Vue from 'vue'
// import Vuex from 'vuex'

import { createStore } from 'vuex'

import ui from './modules/ui/ui'
import ui2 from './modules/ui/ui2'


// Create a new store instance.

const store = createStore({
  state () {
    return {
      count: 0,
      debugmode: false,
      showAppDrawer: null,
      devMode: false
    }
  },
  mutations: {
    increment (state) {
      state.count++
    }
  },
  modules: {
    ui,
    ui2
  }
}) 

app.use(store)