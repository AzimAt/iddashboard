import f from '@/store/fetch.js'

const state = () => ({
  holedetails: {
    ft: null // full table
  }
})

const getters = {

  getHoleDetailsFT: (state) => {
    return state.holedetails.ft
  }

}

const mutations = {

  SET_HOLEDETAILSFT (state, payload) {
    state.holedetails.ft = payload
  },

  RESET_HOLEDETAILSFT (state) {
    state.holedetails.ft = null
  }

}

const actions = {

  async setHoleDetailsFT (context, holeid) {
    context.commit('RESET_HOLEDETAILSFT')
    const url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'HOLE_DETAILS' + '&p1=' + holeid[0]
    f.fget(url)
      .then(res => {
        context.commit('SET_HOLEDETAILSFT', res.content)
      })
  }

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
