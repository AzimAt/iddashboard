import _ from 'lodash'
import f from '@/store/fetch.js'

const state = () => ({
  drillholes: {
    ft: null // full table
  },
  wellstat: {
    ignrdNtExistW: [],
    ignrdExistW: [],
    dsntExistW: [],
    ignrdNtExistWcnt: null,
    ignrdExistWcnt: null,
    dsntExistWcnt: null
  }
})

const getters = {

  getDrillholesFT: (state) => {
    console.log('hi13. getDrillholesFT = ', state.drillholes.ft)
    let dh_strip = JSON.parse(JSON.stringify(state.drillholes.ft))
    console.log('hi14. JSON.parse = ', dh_strip)
    
    return dh_strip //state.drillholes.ft
  },

  getWellStat: (state) => {
    return state.wellstat
  },

  getDistinct: (state) => {
    const drillholesFt = JSON.parse(JSON.stringify(state.drillholes.ft))
    //console.log('hi18. getDistinct.drillholesFt = ', drillholesFt)
    const distinct = {}
    const listOfDistinct = ['HoleStatus', 'XRF_Status', 'HolePurpose', 'WillNotBeGiklet', 'Giklet_status', 'LogStation', 'DB', 'Well_exist', 'Ignored_well']
    _.forEach(listOfDistinct, function (field) {
      //console.log('hi19. field = ', field)
      distinct[field] = []
      _.forEach(drillholesFt, function (value) {
        //console.log('hi20. value = ', value)
        distinct[field] = _.union([value[field]], distinct[field])
      })
    })
    //console.log('hi21. distinct = ', distinct)
    return distinct
  }

}
const mutations = {

  RESET_WELLSTAT (state) {
    state.wellstat.ignrdNtExistW = []
    state.wellstat.ignrdExistW = []
    state.wellstat.dsntExistW = []
    state.wellstat.ignrdNtExistWcnt = null
    state.wellstat.ignrdExistWcnt = null
    state.wellstat.dsntExistWcnt = null
  },

  SET_WELLSTAT (state) {
    state.wellstat.ignrdNtExistW = []
    state.wellstat.ignrdExistW = []
    state.wellstat.dsntExistW = []
    state.wellstat.ignrdNtExistWcnt = null
    state.wellstat.ignrdExistWcnt = null
    state.wellstat.dsntExistWcnt = null

    if (state.drillholes.ft) {
      state.drillholes.ft.forEach(
        function (el) {
          if (el.Ignored_well !== 'Yes' && el.Well_exist === 'Yes') {
            state.wellstat.ignrdNtExistW.push(el.HOLEID)
          }

          if (el.Ignored_well === 'Yes' && el.Well_exist === 'Yes') {
            state.wellstat.ignrdExistW.push(el.HOLEID)
          }

          if (el.Ignored_well !== 'Yes' && el.Well_exist !== 'Yes') {
            state.wellstat.dsntExistW.push(el.HOLEID)
          }
        }
      )
      //   console.log(state.wellstat.ignrdNtExistW[0])
      //   console.log(state.wellstat.ignrdExistW[0])
      //   console.log(state.wellstat.dsntExistW[0])
      state.wellstat.ignrdNtExistWcnt = state.wellstat.ignrdNtExistW.length
      state.wellstat.ignrdExistWcnt = state.wellstat.ignrdExistW.length
      state.wellstat.dsntExistWcnt = state.wellstat.dsntExistW.length
    }
  },

  SET_DRILLHOLESFT (state, payload) {
    state.drillholes.ft = payload
  },

  RESET_DRILLHOLESFT (state) {
    state.drillholes.ft = null
  }

}

const actions = {

  async setDrillholes (context, payload) {
    context.commit('RESET_DRILLHOLESFT')
    context.commit('RESET_WELLSTAT')
    // console.log('hi13. setDrillholes. &p1=' + payload[0] + '&p2=' + payload[1] + '&p3=' + payload[2])
    let url = ''
    if (payload[1] === 'TB_DEM' || payload[1] === 'TB_GEOS' || payload[1] === '3D_GEOS') {
      // console.log('payload[1] == TB_DEM TB_GEOS 3D_GEOS, as ' + payload[1] + ' payload[0] ' + payload[0])
      url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'SHAPE_DRILLHOLES' + '&p1=' + payload[0] + '&p2=' + payload[1] + '&p3=' + payload[2]
    } else if (payload[1] === 'PROF') {
      // console.log('NOT payload[1] == TB_DEM TB_GEOS 3D_GEOS, but ' + payload[1] + ' payload[0] ' + payload[0])
      url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'PROFILE_DRILLHOLES' + '&p1=' + payload[0] + '&p2=' + payload[1] + '&p3=' + payload[2]
    } else {
      // console.log('hi13-1. setDrillholes - DEPOSIT_DRILLHOLES &p1=' + payload[0] + '&p2=' + payload[1] + '&p3=' + payload[2])
      url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'DEPOSIT_DRILLHOLES' + '&p1=' + payload[0] + '&p2=' + payload[1] + '&p3=' + payload[2]
    }
    f.fget(url)
      .then(res => {
        context.commit('SET_DRILLHOLESFT', res.content)
        context.commit('SET_WELLSTAT')
      })
  },
  resetDrillholes (context) {
    context.commit('RESET_DRILLHOLESFT')
    context.commit('RESET_WELLSTAT')
  }

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
