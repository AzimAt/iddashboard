import f from '@/store/fetch.js'

const state = () => ({
  wells: {
    ft: null // full table
  },
  activeWell: null
})

// getters
const getters = {

  getWellsFT: (state) => {
    return state.wells.ft
  },

  getActiveWell: (state) => {
    return state.activeWell
  }
}
// mutations
const mutations = {

  SET_WELLSFT (state, payload) {
    state.wells.ft = payload
  },

  RESET_WELLSFT (state) {
    state.wells.ft = null
  },

  SET_ACTIVEWELL (state, payload) {
    state.activeWell = payload
  },

  RESET_ACTIVEWELL (state) {
    state.activeWell = null
  }

}

// actions
const actions = {

  async setWells (context, payload) {
    context.commit('RESET_WELLSFT')
    let url = ''
    if (payload[1] === 'DEPO') {
      url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'DEPOSIT_DRILLHOLES' + '&p1=' + payload[0] + '&p2=' + payload[1] + '&p3=' + payload[2]
    } else {
      url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'SHAPE_DRILLHOLES' + '&p1=' + payload[0] + '&p2=' + payload[1] + '&p3=' + payload[2]
    }
    f.fget(url)
      .then(res => {
        context.commit('SET_WELLSFT', res.content)
      })
  },

  setActiveWell (context, payload) {
    context.commit('RESET_ACTIVEWELL')
    context.commit('SET_ACTIVEWELL', payload)
  },
  resetWells (context) {
    context.commit('RESET_WELLSFT')
    context.commit('RESET_ACTIVEWELL')
  }

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
