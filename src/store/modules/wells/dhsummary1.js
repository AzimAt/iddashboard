import f from '@/store/fetch.js'

const state = () => ({
  dhsummary1: {
    ft: null // full table
  }
})

// getters
const getters = {

  getDHSummary1FT: (state) => {
    return state.dhsummary1.ft
  }

}
// mutations
const mutations = {

  SET_DHSUMMARY1FT (state, payload) {
    state.dhsummary1.ft = payload
  },

  RESET_DHSUMMARY1FT (state) {
    state.dhsummary1.ft = null
  }

}

// actions
const actions = {

  async setDHSummary1 (context, payload) {
    context.commit('RESET_DHSUMMARY1FT')
    const url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querykv1.aspx?tfn=' + 'SHAPE_DRILLHOLES_NB' + '&p1=' + payload[0] + '&p2=' + payload[1] + '&p3=' + payload[2]
    f.fget(url)
      .then(res => {
        context.commit('SET_DHSUMMARY1FT', res.content)
      })
  }

}

export default {

  namespaced: true,

  state,

  getters,

  actions,

  mutations

}
