import f from '@/store/fetch.js'

const state = () => ({
  dhsummary3: {
    ft: null // full table
  }
})

const getters = {

  getDHSummary3FT: (state) => {
    return state.dhsummary3.ft
  }

}

const mutations = {

  SET_DHSUMMARY3FT (state, payload) {
    state.dhsummary3.ft = payload
  },

  RESET_DHSUMMARY3FT (state) {
    state.dhsummary3.ft = null
  }

}

const actions = {

  async setDHSummary3 (context, payload) {
    context.commit('RESET_DHSUMMARY3FT')
    const url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querykv1.aspx?tfn=' + 'SHAPE_DH_WITH_GIKLET_NB' + '&p1=' + payload[0] + '&p2=' + payload[1] + '&p3=' + payload[2]
    f.fget(url)
      .then(res => {
        context.commit('SET_DHSUMMARY3FT', res.content)
      })
  }

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
