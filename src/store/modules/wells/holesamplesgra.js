// holesamplesgra.js - dublicate of holesamples.js; test url: https://localhost:4343/#/w/MS_KD_4852_1/samples
// Volkov & UNKNOWN url: https://localhost:4343/#/w/MCI01_01_03_3/samples
// Volkov & UNKNOWN url: https://localhost:4343/#/w/MCI01_01_03_3/samples
// Url for jsdoc: https://habr.com/ru/post/572968/
// example of jsDoc format: https://morioh.com/p/98c1acb6c87a

import f from '@/store/fetch.js'
/** @module SamplesGra
 * @description Модуль отображаения результатов проб на Gra */

/** Хранитель таблицы данных
 * @returns {void} */
const state = () => ({
  holeXRFSamplesGra: {
    ft: null // full table
  }
})
// console.log('Hi holesamplesGra.js')

/** Функция модуля Samples - выборка отдельных параметров */
const getters = {
  /**
   * Возвращает массив данных. Member of getters.
   * @function getHoleXRFFTSamplesGra
   * @returns {array} */
  getHoleXRFFTSamplesGra: (state) => {
    return state.holeXRFSamplesGra.ft
  },

  getHoleXRFU: (state) => {
    // creation of U values array by reducing full table
    // a - is our calculated output
    let res = null
    if (state.holeXRFSamplesGra.ft) {
      res = state.holeXRFSamplesGra.ft.reduce(
        // (a, b) => {
        //   if (b > this.minDepth && b < this.maxDepth) a.push({ val: b, main: b % step === 0 })
        (a, item) => {
          a.push({ value: item.U_XRF_ppm, from: parseFloat(item.SAMPFROM), to: parseFloat(item.SAMPTO) })
          return a
        },
        []) // Here should be start state of a, in this case [] - empty array
    }
    return res
  },
  getHoleXRFCa: (state) => {
    // creation of Ca values array by reducing full table
    // a - is our calculated output
    let res = null
    if (state.holeXRFSamplesGra.ft) {
      res = state.holeXRFSamplesGra.ft.reduce(
        (a, item) => {
          a.push({ value: item.Ca_XRF_ppm, from: parseFloat(item.SAMPFROM), to: parseFloat(item.SAMPTO) })
          return a
        },
        []) // Here should be start state of a, in this case [] - empty array
    }
    return res
  },
  getHoleXRFFe: (state) => {
    // creation of Fe values array by reducing full table
    // a - is our calculated output
    let res = null
    if (state.holeXRFSamplesGra.ft) {
      res = state.holeXRFSamplesGra.ft.reduce(
        (a, item) => {
          a.push({ value: item.Fe_XRF_ppm, from: parseFloat(item.SAMPFROM), to: parseFloat(item.SAMPTO) })
          return a
        },
        []) // Here should be start state of a, in this case [] - empty array
    }
    return res
  },
  getHoleXRFTi: (state) => {
    // creation of Ti values array by reducing full table
    // a - is our calculated output
    let res = null
    if (state.holeXRFSamplesGra.ft) {
      res = state.holeXRFSamplesGra.ft.reduce(
        (a, item) => {
          a.push({ value: item.Ti_XRF_ppm, from: parseFloat(item.SAMPFROM), to: parseFloat(item.SAMPTO) })
          return a
        },
        []) // Here should be start state of a, in this case [] - empty array
    }
    return res
  }
}

// example of jsDoc format: https://morioh.com/p/98c1acb6c87a

/** Контроль изменения данных */
const mutations = {
  /** Store array to local state. Member of const mutations.
   * @function SET_HOLEXRFFTSAMPLESGRA
   * @prop {object} [state=default] - default parameter
   * @prop {array} payload - array of samples
   * @returns {void}
   * @author Azim Atentay */
  SET_HOLEXRFFTSAMPLESGRA (state, payload) {
    state.holeXRFSamplesGra.ft = payload
    // console.log('hi SET_HOLEXRFFTSAMPLESGRA')
  },
  /**
   * Set local state to Null. Member of const mutations.
   * @method RESET_HOLEXRFFTSAMPLESGRA
   * @returns {void} */
  RESET_HOLEXRFFTSAMPLESGRA (state) {
    state.holeXRFSamplesGra.ft = null
  }
}

/** actions for getting data from backend */
const actions = {
  /**
   * Обращение к бакенду и заполнение массива данных.
   * Member of const actions.
   * @function setHoleXRFFTSamplesGra
   * @prop {object} [context=default] - default parameter
   * @prop {string} holeid - HoleId, for example 'MS_KD_4581_1'
   * @returns {void}
   * @author Azim Atentay */
  async setHoleXRFFTSamplesGra (context, holeid) {
    // console.log('hi async setHoleXRFFTSamplesGra started')
    context.commit('RESET_HOLEXRFFTSAMPLESGRA')
    // const url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'HOLE_XRF' + '&p1=' + holeid[0] + '&orderby=XRF_FROM'
    const url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'HOLE_SAMPLES_GR' + '&p1=' + holeid[0] + '&orderby=SAMPFROM'
    f.fget(url)
      .then(res => {
        context.commit('SET_HOLEXRFFTSAMPLESGRA', res.content)
      })
    // console.log('hi async setHoleXRFFTSamplesGra finished')
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
