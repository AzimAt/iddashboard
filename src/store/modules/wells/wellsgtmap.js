import f from '@/store/fetch.js'

// file added by YS on 2021-12-24 for https://dev.azure.com/orano/DevOps%20BU%20Mines/_backlogs/backlog/idcard/Features/?workitem=29071

const state = () => ({
  wellsgtmap: {
    ft: null // full table
  }
})

// getters
const getters = {

  getWellsgtmapFT: (state) => {
    return state.wellsgtmap.ft
  }
}
// mutations
const mutations = {

  SET_WELLSGTMAPFT (state, payload) {
    state.wellsgtmap.ft = payload
  },

  RESET_WELLSGTMAPFT (state) {
    state.wellsgtmap.ft = null
  }
}

// actions
const actions = {

  async setWellsgtmap (context, payload) {
    context.commit('RESET_WELLSGTMAPFT')
    const url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'SHAPE_GT_MAP' + '&p1=' + payload[0] + '&p2=' + payload[1] + '&p3=' + payload[2]
    f.fget(url)
      .then(res => {
        context.commit('SET_WELLSGTMAPFT', res.content)
      })
  },
  resetWellsgtmap (context) {
    context.commit('RESET_WELLSGTMAPFT')
  }

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
