/* eslint-disable dot-notation */
import _ from 'lodash'
import f from '@/store/fetch.js'

const state = () => ({
  wellsGt: {
    ft: null, // full table
    source: null, // 'WELL' - for single well, 'TB_DEM' for Technological_blocks version of DEM, 'TB_GEOS' for Technological_blocks version of GEOS, '3D_GEOS' for 3D project version of GEOS
    stat: {
      XYthicknessGrade: [],
      drillholes: [],
      thickness: [],
      grade: [],
      gt: [],
      krrPermeable: [],
      cntDrillholes: null,
      avgKrr: null,
      avgThickness: null,
      avgGrade: null,
      avgGt: null,
      sumGt: null,
      minGt: null,
      maxGt: null
    }
  }
})

// getters
const getters = {

  getWellsGtFT: (state) => {
    return state.wellsGt.ft
  },
  getGtStat: (state) => {
    return state.wellsGt.stat
  },
  getWellGt: (state) => {
    let res = null
    if (state.wellsGt.source === 'WELL') {
    // creation of U values array by reducing full table
    // a - is our calculated output
      if (state.wellsGt.ft) {
        res = state.wellsGt.ft.reduce(
          (res, item) => {
            res.push({ value: item.GRADE, from: parseFloat(item.DEPTH_FROM), to: parseFloat(item.DEPTH_TO) })
            return res
          },
          []) // Here should be start state of a, in this case [] - empty array
      }
    }
    return res
  }

}
// mutations
const mutations = {

  RESET_GTSTAT (state) { // you can add payload if you need
    state.wellsGt.stat.XYthicknessGrade = []
    state.wellsGt.stat.drillholes = []
    state.wellsGt.stat.thickness = []
    state.wellsGt.stat.grade = []
    state.wellsGt.stat.gt = []
    state.wellsGt.stat.krrPermeable = []
    state.wellsGt.stat.cntDrillholes = null
    state.wellsGt.stat.avgKrr = null
    state.wellsGt.stat.avgThickness = null
    state.wellsGt.stat.avgGrade = null
    state.wellsGt.stat.avgGt = null
    state.wellsGt.stat.sumGt = null
    state.wellsGt.stat.minGt = null
    state.wellsGt.stat.maxGt = null
  },

  SET_GTSTAT (state) { // you can add payload if you need
    // do reset in case of
    state.wellsGt.stat.XYthicknessGrade = []
    state.wellsGt.stat.drillholes = []
    state.wellsGt.stat.thickness = []
    state.wellsGt.stat.grade = []
    state.wellsGt.stat.gt = []
    state.wellsGt.stat.krrPermeable = []
    state.wellsGt.stat.cntDrillholes = null
    state.wellsGt.stat.avgKrr = null
    state.wellsGt.stat.avgThickness = null
    state.wellsGt.stat.avgGrade = null
    state.wellsGt.stat.avgGt = null
    state.wellsGt.stat.sumGt = null
    state.wellsGt.stat.minGt = null
    state.wellsGt.stat.maxGt = null
    // do reset in case of
    if (state.wellsGt.ft) {
      _.forEach(state.wellsGt.ft, function (value) {
        state.wellsGt.stat.drillholes = _.union([value['HOLEID']], state.wellsGt.stat.drillholes)
      })
      state.wellsGt.ft.forEach(
        function (el) {
          state.wellsGt.stat.thickness.push(parseFloat(el.THICKNESS))
          state.wellsGt.stat.grade.push(parseFloat(el.GRADE))
          state.wellsGt.stat.gt.push(parseFloat(el.GT_U_mPERC))
          if (el.Permeability === '1') {
            state.wellsGt.stat.XYthicknessGrade.push([parseFloat(el.THICKNESS), parseFloat(el.GRADE)])
            state.wellsGt.stat.krrPermeable.push(parseInt(el.KRR))
          }
        }
      )
      state.wellsGt.stat.avgThickness = (_.sum(state.wellsGt.stat.thickness) / state.wellsGt.stat.thickness.length).toFixed(2)
      state.wellsGt.stat.avgGrade = (_.sum(state.wellsGt.stat.grade) / state.wellsGt.stat.grade.length).toFixed(4)
      state.wellsGt.stat.avgGt = (_.sum(state.wellsGt.stat.gt) / state.wellsGt.stat.gt.length).toFixed(4)
      state.wellsGt.stat.sumGt = _.sum(state.wellsGt.stat.gt).toFixed(4)
      state.wellsGt.stat.minGt = _.min(state.wellsGt.stat.gt).toFixed(4)
      state.wellsGt.stat.maxGt = _.max(state.wellsGt.stat.gt).toFixed(4)
      // remove NAN from array Krr
      const krrPermeableWithoutNAN = state.wellsGt.stat.krrPermeable.filter(function (value) { return !Number.isNaN(value) })
      state.wellsGt.stat.avgKrr = (_.sum(krrPermeableWithoutNAN) / krrPermeableWithoutNAN.length).toFixed(2)
      state.wellsGt.stat.cntDrillholes = state.wellsGt.stat.drillholes.length
    }
  },

  SET_WELLSGTFT (state, payload) {
    state.wellsGt.ft = payload
  },

  RESET_WELLSGTFT (state) {
    state.wellsGt.ft = null
  },

  SET_SOURCE (state, payload) {
    state.wellsGt.source = payload
    // console.log('state.wellsGt.source ' + payload)
  },

  RESET_SOURCE (state) {
    state.wellsGt.source = null
  }
}

// actions
const actions = {

  async setWellsGt (context, payload) {
    context.commit('RESET_GTSTAT')
    context.commit('RESET_WELLSGTFT')
    context.commit('RESET_SOURCE')
    context.commit('SET_SOURCE', payload[1])
    let url = ''
    if (payload[1] !== 'WELL') url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'SHAPE_ATOMGEO_GT' + '&p1=' + payload[0] + '&p2=' + payload[1] + '&p3=' + payload[2] + '&orderby=HOLEID,DEPTH_FROM'
    else url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'WELL_ATOMGEO_GT' + '&p1=' + payload[0] + '&orderby=HOLEID,DEPTH_FROM'
    // (res.result !== 1)
    f.fget(url)
      .then(res => {
        context.commit('SET_WELLSGTFT', res.content)
        if (payload[1] !== 'WELL') context.commit('SET_GTSTAT')
      })
  },
  resetWellsGt (context) {
    context.commit('RESET_GTSTAT')
    context.commit('RESET_WELLSGTFT')
  }

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
