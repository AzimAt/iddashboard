import _ from 'lodash'
import f from '@/store/fetch.js'

const state = () => ({
  dh3dhistory: {
    ft: null // full table
  }
})

const getters = {

  getDH3DhistoryFT: (state) => {
    // console.log('h31.getDH3DhistoryFT. state.dh3dhistory.ft = ', state.dh3dhistory.ft)
    return state.dh3dhistory.ft
  },

  getDH3DhistDistinct: (state) => {
    const distinct = {}
    const listOfDistinct = ['Ignored', 'list_version', 'HoleStatus', 'HolePurpose', 'DB', 'ADD_DATE', 'ADD_USER']
    _.forEach(listOfDistinct, function (field) {
      distinct[field] = []
      _.forEach(state.dh3dhistory.ft, function (value) {
        distinct[field] = _.union([value[field]], distinct[field])
      })
    })

    return distinct
  }

}
const mutations = {

  SET_DH3DHISTORYFT (state, payload) {
    state.dh3dhistory.ft = payload
  },

  RESET_DH3DHSTORYFT (state) {
    state.dh3dhistory.ft = null
  }

}

const actions = {

  async setDH3Dhistory (context, payload) {
    // console.log('h21. setDH3Dhistory, payload = ', payload)
    context.commit('RESET_DH3DHSTORYFT')
    const url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + '3D_DRILLHOLE_LIST' + '&p1=' + payload[0]
    f.fget(url)
      .then(res => {
        // console.log('h22. res.content = ', res.content)
        context.commit('SET_DH3DHISTORYFT', res.content)
      })
  }

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
