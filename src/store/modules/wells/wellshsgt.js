import f from '@/store/fetch.js'

const state = () => ({
  wellsHsGt: {
    ft: null // full table
  }
})

// getters
const getters = {

  getWellsHsGtFT: (state) => {
    return state.wellsHsGt.ft
  }

}
// mutations
const mutations = {

  SET_WELLSHSGTFT (state, payload) {
    state.wellsHsGt.ft = payload
  },

  RESET_WELLSHSGTFT (state) {
    state.wellsHsGt.ft = null
  }

}

// actions
const actions = {

  async setWellsHsGt (context, payload) {
    context.commit('RESET_WELLSHSGTFT')
    const url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'SHAPE_HISTORICAL_GT' + '&p1=' + payload[0] + '&p2=' + payload[1] + '&p3=' + payload[2] + '&orderby=HOLEID,DEPTH_FROM'
    f.fget(url)
      .then(res => {
        context.commit('SET_WELLSHSGTFT', res.content)
      })
  },
  resetWellsHsGt (context) {
    context.commit('RESET_WELLSHSGTFT')
  }
}

export default {

  namespaced: true,

  state,

  getters,

  actions,

  mutations

}
