// holesamples.js - Карбонаты
// dublicate of holesamples.js; test url: https://localhost:4343/#/w/MS_KD_4852_1/samples
// Volkov & UNKNOWN url: https://localhost:4343/#/w/MCI01_01_03_3/samples
// Volkov & UNKNOWN url: https://localhost:4343/#/w/MCI01_01_03_3/samples
// Url for jsdoc: https://habr.com/ru/post/572968/
// example of jsDoc format: https://morioh.com/p/98c1acb6c87a

import f from '@/store/fetch.js'
/** @module Samples
 * @description Модуль отображаения результатов проб на URa
*/

/** Хранитель таблицы данных
 * @returns {void} */
const state = () => ({
  holeXRFSamples: {
    ft: null // full table
  }
})
// console.log('Hi holesamples.js')

/** Функция модуля Samples - выборка отдельных параметров */
const getters = {
  /**
   * Возвращает массив данных
   * @function getHoleXRFFTSamples
   * @returns {array}
  */
  getHoleXRFFTSamples: (state) => {
    return state.holeXRFSamples.ft
  },

  getHoleXRFUSamples: (state) => {
    // creation of U values array by reducing full table
    // a - is our calculated output
    // console.log('hi getHoleXRFUSamples')
    let res = null
    // console.log('state.holeXRFSamples.ft = ' + state.holeXRFSamples.ft)
    if (state.holeXRFSamples.ft) {
      res = state.holeXRFSamples.ft.reduce(
        // (a, b) => {
        //   if (b > this.minDepth && b < this.maxDepth) a.push({ val: b, main: b % step === 0 })
        (a, item) => {
          a.push({ value: item.U_XRF_pct, from: parseFloat(item.SAMPFROM), to: parseFloat(item.SAMPTO) })
          // console.log('item.U_XRF_pct:' + item.U_XRF_pct + ', from:' + parseFloat(item.SAMPFROM))
          return a
        },
        []) // Here should be start state of a, in this case [] - empty array
    }
    return res
  },
  getHoleXRF: (state) => {
    // creation of Ca values array by reducing full table
    // a - is our calculated output
    let res = null
    if (state.holeXRFSamples.ft) {
      res = state.holeXRFSamples.ft.reduce(
        (a, item) => {
          a.push({ value: item.Ca_XRF_ppm, from: parseFloat(item.SAMPFROM), to: parseFloat(item.SAMPTO) })
          return a
        },
        []) // Here should be start state of a, in this case [] - empty array
    }
    return res
  },
  getHoleXRFFe: (state) => {
    // creation of Fe values array by reducing full table
    // a - is our calculated output
    let res = null
    if (state.holeXRFSamples.ft) {
      res = state.holeXRFSamples.ft.reduce(
        (a, item) => {
          a.push({ value: item.Fe_XRF_ppm, from: parseFloat(item.SAMPFROM), to: parseFloat(item.SAMPTO) })
          return a
        },
        []) // Here should be start state of a, in this case [] - empty array
    }
    return res
  },
  getHoleXRFTi: (state) => {
    // creation of Ti values array by reducing full table
    // a - is our calculated output
    let res = null
    if (state.holeXRFSamples.ft) {
      res = state.holeXRFSamples.ft.reduce(
        (a, item) => {
          a.push({ value: item.Ti_XRF_ppm, from: parseFloat(item.SAMPFROM), to: parseFloat(item.SAMPTO) })
          return a
        },
        []) // Here should be start state of a, in this case [] - empty array
    }
    return res
  }
}

// example of jsDoc format: https://morioh.com/p/98c1acb6c87a

/** Контроль изменения данных */
const mutations = {
  /** Store array to local state. Member of const mutations.
   * @function SET_HOLEXRFFTSAMPLES
   * @prop {object} [state=default] - default parameter
   * @prop {array} payload - array of samples
   * @returns {void}
   * @author Azim Atentay */
  SET_HOLEXRFFTSAMPLES (state, payload) {
    state.holeXRFSamples.ft = payload
    // console.log('hi SET_HOLEXRFFTSAMPLES')
  },
  /**
   * Set local state to Null. Member of const mutations.
   * @method RESET_HOLEXRFFTSAMPLES
   * @returns {void}
  */
  RESET_HOLEXRFFTSAMPLES (state) {
    state.holeXRFSamples.ft = null
  }
}

/** actions for getting data from backend */
const actions = {
  /**
   * Обращение к бакенду и заполнение массива данных.
   * Member of const actions.
   * @function setHoleXRFFTSamples
   * @prop {object} [context=default] - default parameter
   * @prop {string} holeid - HoleId, for example 'MS_KD_4581_1'
   * @returns {void}
   * @author Azim Atentay */
  async setHoleXRFFTSamples (context, holeid) {
    // console.log('hi async setHoleXRFFTSamples started')
    // context.commit('RESET_HOLEXRFFTSAMPLES')
    // const url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'HOLE_XRF' + '&p1=' + holeid[0] + '&orderby=XRF_FROM'
    const url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'HOLE_SAMPLES' + '&p1=' + holeid[0] + '&orderby=SAMPFROM'
    f.fget(url)
      .then(res => {
        context.commit('SET_HOLEXRFFTSAMPLES', res.content)
      })
    // console.log('hi async setHoleXRFFTSamples finished')
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
