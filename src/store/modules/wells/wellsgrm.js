import f from '@/store/fetch.js'

const state = () => ({
  wellsGrm: {
    ft: null // full table
  }
})

// getters
const getters = {

  getWellsGrmFT: (state) => {
    return state.wellsGrm.ft
  }

}
// mutations
const mutations = {

  SET_WELLSGRMFT (state, payload) {
    state.wellsGrm.ft = payload
  },

  RESET_WELLSGRMFT (state) {
    state.wellsGrm.ft = null
  }

}

// actions
const actions = {

  async setWellsGrm (context, payload) {
    context.commit('RESET_WELLSGRMFT')
    const url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'SHAPE_ATOMGEO_GRM' + '&p1=' + payload[0] + '&p2=' + payload[1] + '&p3=' + payload[2]
    f.fget(url)
      .then(res => {
        context.commit('SET_WELLSGRMFT', res.content)
      })
  }
}

export default {

  namespaced: true,

  state,

  getters,

  actions,

  mutations

}
