// import _ from 'lodash'
import f from '@/store/fetch.js'

const state = () => ({
  holeXRF: {
    ft: null // full table
  }
})

const getters = {

  getHoleXRFFT: (state) => {
    return state.holeXRF.ft
  },

  getHoleXRFU: (state) => {
    // creation of U values array by reducing full table
    // a - is our calculated output
    let res = null
    if (state.holeXRF.ft) {
      res = state.holeXRF.ft.reduce(
        // (a, b) => {
        //   if (b > this.minDepth && b < this.maxDepth) a.push({ val: b, main: b % step === 0 })
        (a, item) => {
          a.push({ value: item.U_XRF_ppm, from: parseFloat(item.XRF_FROM), to: parseFloat(item.XRF_TO) })
          return a
        },
        []) // Here should be start state of a, in this case [] - empty array
    }
    return res
  },
  getHoleXRFCa: (state) => {
    // creation of Ca values array by reducing full table
    // a - is our calculated output
    let res = null
    if (state.holeXRF.ft) {
      res = state.holeXRF.ft.reduce(
        (a, item) => {
          a.push({ value: item.Ca_XRF_ppm, from: parseFloat(item.XRF_FROM), to: parseFloat(item.XRF_TO) })
          return a
        },
        []) // Here should be start state of a, in this case [] - empty array
    }
    return res
  },
  getHoleXRFFe: (state) => {
    // creation of Fe values array by reducing full table
    // a - is our calculated output
    let res = null
    if (state.holeXRF.ft) {
      res = state.holeXRF.ft.reduce(
        (a, item) => {
          a.push({ value: item.Fe_XRF_ppm, from: parseFloat(item.XRF_FROM), to: parseFloat(item.XRF_TO) })
          return a
        },
        []) // Here should be start state of a, in this case [] - empty array
    }
    return res
  },
  getHoleXRFTi: (state) => {
    // creation of Ti values array by reducing full table
    // a - is our calculated output
    let res = null
    if (state.holeXRF.ft) {
      res = state.holeXRF.ft.reduce(
        (a, item) => {
          a.push({ value: item.Ti_XRF_ppm, from: parseFloat(item.XRF_FROM), to: parseFloat(item.XRF_TO) })
          return a
        },
        []) // Here should be start state of a, in this case [] - empty array
    }
    return res
  }
}

const mutations = {

  SET_HOLEXRFFT (state, payload) {
    state.holeXRF.ft = payload
  },

  RESET_HOLEXRFFT (state) {
    state.holeXRF.ft = null
  }

}

const actions = {
  // querying XRF picking data for the well
  async setHoleXRFFT (context, holeid) {
    context.commit('RESET_HOLEXRFFT')
    const url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'HOLE_XRF' + '&p1=' + holeid[0] + '&orderby=XRF_FROM'
    f.fget(url)
      .then(res => {
        context.commit('SET_HOLEXRFFT', res.content)
      })
  }

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
