import f from '@/store/fetch.js'

const state = () => ({
  dhsummary2: {
    ft: null // full table
  }
})

// getters
const getters = {

  getDHSummary2FT: (state) => {
    return state.dhsummary2.ft
  }

}
// mutations
const mutations = {

  SET_DHSUMMARY2FT (state, payload) {
    state.dhsummary2.ft = payload
  },

  RESET_DHSUMMARY2FT (state) {
    state.dhsummary2.ft = null
  }

}

// actions
const actions = {

  async setDHSummary2 (context, payload) {
    context.commit('RESET_DHSUMMARY2FT')
    const url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querykv1.aspx?tfn=' + 'SHAPE_DHLOGGING_NB' + '&p1=' + payload[0] + '&p2=' + payload[1] + '&p3=' + payload[2]
    f.fget(url)
      .then(res => {
        context.commit('SET_DHSUMMARY2FT', res.content)
      })
  }

}

export default {

  namespaced: true,

  state,

  getters,

  actions,

  mutations

}
