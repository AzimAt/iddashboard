import f from '@/store/fetch.js'

const state = () => ({
  wells: {
    ft: null // full table
  },
  activeWell: null
})

// getters
const getters = {

  getWellsPIACQFT: (state) => {
    return state.wells.ft
  },

  getActiveWell: (state) => {
    return state.activeWell
  }
}
// mutations
const mutations = {

  SET_WELLSPIACQFT (state, payload) {
    state.wells.ft = payload
  },

  RESET_WELLSPIACQFT (state) {
    state.wells.ft = null
  },

  SET_ACTIVEWELL (state, payload) {
    state.activeWell = payload
  },

  RESET_ACTIVEWELL (state) {
    state.activeWell = null
  }

}

// actions
const actions = {

  async setWellsPIACQ (context, payload) {
    context.commit('RESET_WELLSPIACQFT')
    const url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'SHAPE_DRILLHOLES_PIACQ' + '&p1=' + payload[0] + '&p2=' + payload[1] + '&p3=' + payload[2]
    f.fget(url)
      .then(res => {
        context.commit('SET_WELLSPIACQFT', res.content)
      })
  },

  setActiveWell (context, payload) {
    context.commit('RESET_ACTIVEWELL')
    context.commit('SET_ACTIVEWELL', payload)
  },
  resetWellsPIACQ (context) {
    context.commit('RESET_WELLSPIACQFT')
    context.commit('RESET_ACTIVEWELL')
  }

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
