import f from '@/store/fetch.js'

const state = () => ({
  // qcresultsbydeposit: {
  depositsamples: {
    ft: null // full table
  }
})

const getters = {

  // getQCresultsbydeposit: (state) => {
  getDepositsamples: (state) => {
    return state.depositsamples.ft
  }

}
const mutations = {

  // SET_QCRESULTSBYDEPOSIT (state, payload) {
  SET_DEPOSITSAMPLES (state, payload) {
    state.depositsamples.ft = payload
  },

  RESET_DEPOSITSAMPLES (state) {
    state.depositsamples.ft = null
  }

}

const actions = {

  // async setQCresultsbydeposit (context, payload) {
  async setDepositsamples (context, payload) {
    context.commit('RESET_DEPOSITSAMPLES')
    const url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=DEPOSIT_BLOCKS_RESERVE&&p1=' + payload[0]
    f.fget(url)
      .then(res => {
        context.commit('SET_DEPOSITSAMPLES', res.content)
      })
  },
  // resetQCresultsbydeposit (context) {
  resetdepositsamples (context) {
    context.commit('RESET_DEPOSITSAMPLES')
  }

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
