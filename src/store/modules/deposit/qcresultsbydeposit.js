import f from '@/store/fetch.js'

const state = () => ({
  qcresultsbydeposit: {
    ft: null // full table
  }
})

const getters = {

  getQCresultsbydeposit: (state) => {
    return state.qcresultsbydeposit.ft
  }

}
const mutations = {

  SET_QCRESULTSBYDEPOSIT (state, payload) {
    state.qcresultsbydeposit.ft = payload
  },

  RESET_QCRESULTSBYDEPOSIT (state) {
    state.qcresultsbydeposit.ft = null
  }

}

const actions = {

  async setQCresultsbydeposit (context, payload) {
    context.commit('RESET_QCRESULTSBYDEPOSIT')
    let url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytv2.aspx?view=QC_V_ALL_REPORT_BY_DEPOSIT'
    url = url + '&where_field_for_equal=DEPOSIT' // field for where
    url = url + '&where_value_for_equal=' + payload[0] // value for where, '' will be added in tv2.cs
    url = url + '&orderby=CDATE%20DESC,[DATABASE],CATEGORY_FK,QUERY_PK'
    f.fget(url)
      .then(res => {
        context.commit('SET_QCRESULTSBYDEPOSIT', res.content)
      })
  },
  resetQCresultsbydeposit (context) {
    context.commit('RESET_QCRESULTSBYDEPOSIT')
  }

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
