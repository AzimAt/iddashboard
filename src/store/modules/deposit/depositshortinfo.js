// import f from '@/store/fetch.js'

const state = () => ({
  tbSdeAttrFT: {
    ft: null // full table
  }
})

const getters = {
  getDepoShortIFT: (state) => {
    return state.tbSdeAttrFT.ft
  }
}

const mutations = {

  SET_TBDEPOSHORTIFT (state, payload) {
    state.tbSdeAttrFT.ft = payload
  },

  RESET_TBDEPOSHORTIFT (state) {
    state.tbSdeAttrFT.ft = null
  }

}

const actions = {
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
