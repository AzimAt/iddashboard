import f from '@/store/fetch.js'

const state = () => ({
  deposits: {
    ft: null // full table
  }
})

const getters = {

  getDeposits: (state) => {
    return state.deposits.ft
  }

}
const mutations = {

  SET_DEPOSITS (state, payload) {
    state.deposits.ft = payload
  },

  RESET_DEPOSITS (state) {
    state.deposits.ft = null
  }

}

const actions = {

  async setDeposits (context, payload) {
    context.commit('RESET_DEPOSITS')
    const url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'DEPOSITS' + '&orderby=name'
    f.fget(url)
      .then(res => {
        context.commit('SET_DEPOSITS', res.content)
      })
  },
  resetDeposits (context) {
    context.commit('RESET_DEPOSITS')
  }

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
