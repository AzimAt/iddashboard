import f from '@/store/fetch.js'

const state = () => ({
  dhs: {
    ft: null // full table
  }
})

// getters
const getters = {

  getDHsFT: (state) => {
    return state.dhs.ft
  }

}
// mutations
const mutations = {

  SET_DHSFT (state, payload) {
    state.dhs.ft = payload
  },

  RESET_DHSFT (state) {
    state.dhs.ft = null
  }

}

// actions
const actions = {

  async setDHs (context) {
    context.commit('RESET_DHSFT')
    const url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'DHS' + '&orderby=' + 'id'
    f.fget(url)
      .then(res => {
        context.commit('SET_DHSFT', res.content)
      })
  }

}

export default {

  namespaced: true,

  state,

  getters,

  actions,

  mutations

}
