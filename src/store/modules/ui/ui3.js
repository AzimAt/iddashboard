import f from '@/store/fetch.js'
// console.log('hiUi3. ui3.js')
const state = () => ({
  wellDataDropDownList: {
    ft: null // full table
  }
})

// getters
const getters = {
  getWellDataDropDownList (state) {
    const res = state.wellDataDropDownList.ft
    return res
  }
}

// mutations
const mutations = {
  SET_WELLDATADROPDOWNLIST (state, payload) {
    state.wellDataDropDownList.ft = payload
  }

}

// actions
const actions = {
  async setWellDataDropDownList (context) {
    const url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytv1.aspx?view=WIKI_ACQ_PRIORITIES&where=UI_string%20is%20not%20null&fields=rid,DatabaseName,TableName,VirtualFieldName,PriorityMatchOperation,PRIORITY,UI_string,UI_help_string'
    f.fget(url)
      .then(res => {
        context.commit('SET_WELLDATADROPDOWNLIST', res.content)
      })
  }

}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
