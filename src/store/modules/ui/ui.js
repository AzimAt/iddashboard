const state = () => ({
  active: {
    page: {
      name: '',
      url: '',
      version: ''
    } // page
  } // active
})

// getters, actions,mutations removed for avoid of errors in VS Code

export default {
  namespaced: false,
  state
}
