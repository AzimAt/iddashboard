import f from '@/store/fetch.js'
// console.log('h0.ui2.js')
const state = () => ({
  shapeType: {
    type: null,
    caption: null,
    url: null,
    lastPage: null
  },
  pathP1: null,
  appVersion: '3.0.1', // depricated parameter. Write version into package.json
  user: null,
  userrights: null,
})

// getters
const getters = {
  getUserRights (state) { return state.userrights },
  getShapeType (state) {
    return state.shapeType
  },
  getPathP1 (state) {
    return state.pathP1
  },
  getCaption (state) {
    // TO DO: ADD 'PROF'
    let res = ' of ' + state.shapeType.caption + ' ' + state.pathP1
    if ((state.shapeType.type === 'TB_DEM') || (state.shapeType.type === 'TB_GEOS')) res = res + ' with the buffer 20 m'
    return res
  },
  getAppVersion (state) { return state.appVersion },
  getUser (state) { return state.user }
}

// mutations
const mutations = {
  // appliedShapeText
  SET_APPLIEDSHAPETEXT (state, payload) {
    state.appliedShapeText = payload
  },
  SET_SHAPETYPE (state, payload) {
    // console.log('SET_SHAPETYPE = ', payload)
    state.shapeType = payload
  },
  SET_PATHP1 (state, payload) {
    state.pathP1 = payload
  },
  SET_USER (state, payload) {
    state.user = payload
  },
  SET_USERRIGHTS (state, payload) {
    state.userrights = payload
  }
}

// actions
const actions = {
  setShapeType (context, p) {
    // console.log('h1. ui2. setShapeType,  p = ', p)
    let lastpage = p
    lastpage = p.substr(p.lastIndexOf('/') + 1, p.length - p.lastIndexOf('/'))
    p = p.substr(1, p.length - 2)
    p = p.substr(0, p.indexOf('/'))
    // console.log('h2. ui2. setShapeType. p= ', p)
    const res = {
      type: null,
      caption: null,
      url: null,
      lastPage: null
    }
    if (p === 'b') {
      res.type = 'TB_DEM'
      res.caption = 'Tech. Block'
    } else if (p === 'g3d') {
      res.type = '3D_GEOS'
      res.caption = '3D Model'
    } else if (p === 'gtb') {
      res.type = 'TB_GEOS'
      res.caption = 'Tech. Block Estimation'
    } else if (p === 'prof') {
      res.type = 'PROF'
      res.caption = 'Profile'
    } else if (p === 'depo') {
      res.type = 'DEPO'
      res.caption = 'Deposit'
    } else if (p === 'w') {
      res.type = 'WELL'
      res.caption = 'Well'
    } else if (p === 'r') {
      res.type = 'REPORT'
      res.caption = 'Report'
    } else if (p === 's') {
      res.type = 'SAMPLING'
      res.caption = 'Sampling'
    }
    res.url = p
    res.lastPage = lastpage
    context.commit('SET_SHAPETYPE', res)
  },
  setPathP1 (context, p) {
    context.commit('SET_PATHP1', p)
  },
  async setUser (context) {
    // console.log('h6. setUser started')
    const url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'USER_ACCESS'
    f.fget(url)
      .then(res => {
        // console.log('h1. res.content = ', res.content)
        context.commit('SET_USER', res.content[0])
      })
  },
  setUserRights (context) {
    // console.log('setUserRights started')
    const url2 = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=PORTAL_ALLOWEDROUTES_BY_USERNAME&p1=NULL'
    f.fget(url2)
      .then(res2 => {
        context.commit('SET_USERRIGHTS', res2.content)
        // console.log('h2. SET_USERRIGHTS = ', res2.content)
      })
  }

}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
