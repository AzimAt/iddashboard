import f from '@/store/fetch.js'

const state = () => ({
  geos3d: {
    ft: null // full table
  }
})

// getters
const getters = {

  getGEOS3dFT: (state) => {
    return state.geos3d.ft
  }

}
// mutations
const mutations = {

  SET_GEOS3DFT (state, payload) {
    state.geos3d.ft = payload
  },

  RESET_GEOS3DFT (state) {
    state.geos3d.ft = null
  }

}

// actions
const actions = {

  async setGEOS3d (context) {
    context.commit('RESET_GEOS3DFT')
    const url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + '3D_GEOS_LIST'
    f.fget(url)
      .then(res => {
        context.commit('SET_GEOS3DFT', res.content)
      })
  }

}

export default {

  namespaced: true,

  state,

  getters,

  actions,

  mutations

}
