import f from '@/store/fetch.js'

const state = () => ({
  tbSdeAttrFT: {
    ft: null // full table
  }
})

const getters = {
  gettbSdeAttrFT: (state) => {
    return state.tbSdeAttrFT.ft
  }
}

const mutations = {

  SET_TBSDEATTRFT (state, payload) {
    state.tbSdeAttrFT.ft = payload
  },

  RESET_TBSDEATTRFT (state) {
    state.tbSdeAttrFT.ft = null
  }

}

const actions = {

  async settbSdeAttrFT (context, payload) {
    context.commit('RESET_TBSDEATTRFT')
    let url = ''
    if (payload[1] === 'TB_DEM') {
      url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'BLOCK_SDE_ATTR' + '&p1=' + payload[0]
    } else if (payload[1] === 'TB_GEOS') {
      url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'BLOCK_SDE_ATTR_ESTIM' + '&p1=' + payload[0]
    } else if (payload[1] === '3D_GEOS') {
      url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'BLOCK_SDE_ATTR_3D' + '&p1=' + payload[0]
    }
    f.fget(url)
      .then(res => {
        context.commit('SET_TBSDEATTRFT', res.content)
      })
  }

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
