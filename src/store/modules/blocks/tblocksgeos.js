import f from '@/store/fetch.js'

const state = () => ({
  tBlocksGEOS: null
})

// getters
const getters = {

  getTBlocksGEOS: (state) => {
    return state.tBlocksGEOS
  }

}
// mutations
const mutations = {

  SET_TBLOCKSGEOS (state, payload) {
    state.tBlocksGEOS = payload
  },

  RESET_TBLOCKSGEOS (state) {
    state.tBlocksGEOS = null
  }

}

// actions
const actions = {

  async setTBlocksGEOS (context) {
    context.commit('RESET_TBLOCKSGEOS')
    const url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'TECHBLOCKS_GEOS_LIST'
    f.fget(url)
      .then(res => {
        context.commit('SET_TBLOCKSGEOS', res.content)
      })
  }

}

export default {

  namespaced: true,

  state,

  getters,

  actions,

  mutations

}
