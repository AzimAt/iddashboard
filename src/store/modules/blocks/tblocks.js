// BEGIN importing axios and set settings because we cannot use plugin in store
import axios from 'axios'
const ax = axios.create({ timeout: 60000, withCredentials: true, headers: { Accept: 'application/json', 'Content-Type': 'application/json' } })
// END of importing axios and set settings because we cannot use plugin in store

const state = () => ({
  tBlocks: null,
  tBlocksBounds: null,
  tDeposBounds: null,
  geos3DBounds: null
})

// getters
const getters = {

  getTBlocks: (state) => {
    // return state.tBlocks
    return state.tBlocks
  },

  getTBlocksBounds: (state) => {
    return  state.tBlocksBounds
  },

  getTDeposBounds: (state) => {
    return state.tDeposBounds
  },
  getGeos3DBounds: (state) => {
    return state.geos3DBounds
  }

}

// mutations
const mutations = {

  SET_TBLOCKS (state, payload) {
    state.tBlocks = payload
  },

  SET_TBLOCKSBOUNDS (state, payload) {
    state.tBlocksBounds = payload
  },

  SET_TDEPOSBOUNDS (state, payload) {
    state.tDeposBounds = payload
  },
  SET_GEOS3DBOUNDS (state, payload) {
    state.geos3DBounds = payload
  }

}

// actions
const actions = {

  async setTBlocks (context) {
    const res = await ax.get(
      'https://gis-katco.bdom.ad.corp/asp/geos/idcard/tblocks.aspx?db=GEOS_EXTRA_QUAL')
    context.commit('SET_TBLOCKS', res.data.content)
  },

  async setTBlocksBounds (context) {
    const res = await ax.get(
      'https://gis-katco.bdom.ad.corp/asp/geos/idcard/tblocksbounds.aspx?db=GEOS_EXTRA_QUAL')
    context.commit('SET_TBLOCKSBOUNDS', res.data.content)
  },
  async setTDeposBounds (context) {
    const res = await ax.get(
      'https://gis-katco.bdom.ad.corp/asp/geos/idcard/tblocksbounds.aspx?db=GEOS_EXTRA_QUAL&type=depo')
    context.commit('SET_TDEPOSBOUNDS', res.data.content)
  },
  async setGeos3DBounds (context) {
    const res = await ax.get(
      'https://gis-katco.bdom.ad.corp/asp/geos/idcard/geos3dbounds.aspx?db=GEOS_EXTRA_QUAL')
    context.commit('SET_GEOS3DBOUNDS', res.data.content)
  }

}

export default {

  namespaced: true,

  state,

  getters,

  actions,

  mutations

}
