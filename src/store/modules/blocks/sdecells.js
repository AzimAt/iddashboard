import f from '@/store/fetch.js'

const state = () => ({
  tbSdeCells: {
    ft: null // full table
  }
})

const getters = {

  gettbSdeCellsFT: (state) => {
    return state.tbSdeCells.ft
  }
}

const mutations = {

  SET_TBSDECELLSFT (state, payload) {
    state.tbSdeCells.ft = payload
  },

  RESET_TBSDECELLSFT (state) {
    state.tbSdeCells.ft = null
  }

}

const actions = {

  async settbSdeCellsFT (context, tblock) {
    context.commit('RESET_TBSDECELLSFT')
    const url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'BLOCK_CELLS' + '&p1=' + tblock
    f.fget(url)
      .then(res => {
        context.commit('SET_TBSDECELLSFT', res.content)
      })
  }

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
