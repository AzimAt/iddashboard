import f from '@/store/fetch.js'
// added by AN for osidem QC for https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=BLOCK_INJECTING_PRODUCERS&p1=%
const state = () => ({
  tbInjectingProducers: {
    ft: null // full table
  },
  tbInjectingProducersNotAddition: {
    ft: null // full table
  },
  tbInjectingProducersResult: null,
  tbInjectingProducersNotAdditionResult: null
})

const getters = {

  getInjectingProducersFT: (state) => {
    return state.tbInjectingProducers.ft
  },
  getInjectingProducersResult: (state) => {
    return state.tbInjectingProducersResult
  },
  getInjectingProducersNotAddition: (state) => {
    return state.tbInjectingProducersNotAddition.ft
  },
  getInjectingProducersNotAdditionResult: (state) => {
    return state.tbInjectingProducersNotAdditionResult
  }

}

const mutations = {

  SET_TB_INJECTING_PRODUCERS_FT (state, payload) {
    state.tbInjectingProducers.ft = payload
  },
  SET_TB_INJECTING_PRODUCERS_RESULT (state, payload) {
    if (payload.length === 0) { payload = null }
    state.tbInjectingProducersResult = payload
  },
  RESET_TB_INJECTING_PRODUCERS_FT (state) {
    state.tbInjectingProducers.ft = null
  },
  RESET_TB_INJECTING_PRODUCERS_RESULT (state) {
    state.tbInjectingProducersResult = null
  },
  SET_TB_INJECTING_PRODUCERS_NOT_ADDITION_FT (state, payload) {
    state.tbInjectingProducersNotAddition.ft = payload
  },
  SET_TB_INJECTING_PRODUCERS_NOT_ADDITION_RESULT (state, payload) {
    if (payload.length === 0) { payload = null }
    state.tbInjectingProducersNotAdditionResult = payload
  },
  RESET_TB_INJECTING_PRODUCERS_NOT_ADDITION_FT (state) {
    state.tbInjectingProducersNotAddition.ft = null
  },
  RESET_TB_INJECTING_PRODUCERS_NOT_ADDITION_RESULT (state) {
    state.tbInjectingProducersNotAdditionResult = null
  }
}

const actions = {

  async setInjectingProducersFT (context, payload) {
    context.commit('RESET_TB_INJECTING_PRODUCERS_FT')
    context.commit('RESET_TB_INJECTING_PRODUCERS_RESULT')
    context.commit('RESET_TB_INJECTING_PRODUCERS_NOT_ADDITION_FT')
    context.commit('RESET_TB_INJECTING_PRODUCERS_NOT_ADDITION_RESULT')
    const p1 = payload[0]
    const p2 = 'PRODUCER' // payload[1]
    const p3 = 'P_INJECTING_FROM_PR_COLLECTOR' // payload[0]
    // const url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'BLOCK_INJECTING_PRODUCERS' + '&p1=' + p1 + '&p2=' + payload[1]
    // --SELECT * FROM [rr].[IC_TFN_BLOCK_INJECTING_PRODUCERS]('MSK25') ORDER BY HOLEID, EVENT_TYPE,ENDTIME
    // --SELECT * FROM [rr].[IC_TFN_BLOCK_INJECTING_PRODUCERS]('') ORDER BY HOLEID, EVENT_TYPE,ENDTIME
    // --Attention ! Some producers with HolePath 'ADDITION' were injecting !
    // --https://localhost:4343/#/b/MSK03/qc/osidem1
    const url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'BLOCK_INJECTING_PRODUCERS' + '&p1=' + p1 + '&p2=' + p2 + '&p3=' + p3
    f.fget(url)
      .then(res => {
        context.commit('SET_TB_INJECTING_PRODUCERS_FT', res.content)
        context.commit('SET_TB_INJECTING_PRODUCERS_RESULT', res.result)
      })
    // const url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'BLOCK_INJECTING_PRODUCERS_NOT_ADDITION' + '&p1=' + p1 + '&p2=' + payload[1]
    // --SELECT * FROM [rr].[IC_TFN_BLOCK_INJECTING_PRODUCERS_NOT_ADDITION]('MSK25') ORDER BY HOLEID, EVENT_TYPE,ENDTIME
    // --SELECT * FROM [rr].[IC_TFN_BLOCK_INJECTING_PRODUCERS_NOT_ADDITION]('') ORDER BY HOLEID, EVENT_TYPE,ENDTIME
    // --Attention ! Some producers with HolePath not equal 'ADDITION' were injecting !
    // --https://localhost:4343/#/b/MSK03/qc/osidem1
    // const url2 = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'BLOCK_INJECTING_PRODUCERS_NOT_ADDITION' + '&p1=' + p1
    // f.fget(url2)
    //   .then(res => {
    //     context.commit('RESET_TB_INJECTING_PRODUCERS_NOT_ADDITION_FT', res.content)
    //     context.commit('RESET_TB_INJECTING_PRODUCERS_NOT_ADDITION_RESULT', res.result)
    //   })
  },
  resetInjectingProducersFT (context) {
    context.commit('RESET_TB_INJECTING_PRODUCERS_FT')
    context.commit('RESET_TB_INJECTING_PRODUCERS_RESULT')
    context.commit('RESET_TB_INJECTING_PRODUCERS_NOT_ADDITION_FT')
    context.commit('RESET_TB_INJECTING_PRODUCERS_NOT_ADDITION_RESULT')
  }

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
