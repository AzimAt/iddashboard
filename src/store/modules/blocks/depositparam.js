import f from '@/store/fetch.js'

const state = () => ({
  tbDepositParams: {
    ft: null // full table
  }
})

const getters = {

  gettbDepositParamsFT: (state) => {
    return state.tbDepositParams.ft
  }
}

const mutations = {

  SET_TBDEPOSITPARAMSFT (state, payload) {
    if (payload.length === 0) { payload = null }
    state.tbDepositParams.ft = payload
  },

  RESET_TBDEPOSITPARAMSFT (state) {
    state.tbDepositParams.ft = null
  }

}

const actions = {

  async settbDepositParamsFT (context, payload) {
    context.commit('RESET_TBDEPOSITPARAMSFT')
    let p1 = payload[0]
    if (p1.indexOf('_') > -1) { p1 = p1.substring(0, p1.indexOf('_')) }
    const url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'DEPOSIT_PARAM' + '&p1=' + p1 + '&p2=' + payload[1]
    f.fget(url)
      .then(res => {
        context.commit('SET_TBDEPOSITPARAMSFT', res.content)
      })
  }

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
