import f from '@/store/fetch.js'
import moment from 'moment'

const state = () => ({
  bHistory: {
    ft: null, // full table
    shapes: null,
    block: {
      shape: null
    }
  }
})

const getters = {

  getBHistoryFT: (state) => {
    return state.bHistory.ft
  },

  getBHShapes: (state) => {
    return state.bHistory.shapes
  },
  getBlockShape: (state) => {
    return state.bHistory.block.shape
  }
}
// mutations
const mutations = {

  SET_BHISTORYFT (state, payload) {
    state.bHistory.ft = payload
  },

  RESET_BHISTORYFT (state) {
    state.bHistory.ft = null
  },

  RESET_BHSHAPES (state) {
    state.bHistory.shapes =
    {
      dates: [],
      geoJSONs: []
    }
    state.bHistory.shapes.dates = []
    state.bHistory.shapes.geoJSONs = []
  },

  SET_BHSHAPES (state) { // you can add payload if you need
    if (state.bHistory.ft) {
      state.bHistory.ft.forEach(el => state.bHistory.shapes.dates.push(
        {
          // date: moment(el.GDB_FROM_DATE).format('ll'), *depricated function. Azim. 2024-01-22
          date: moment(el.GDB_FROM_DATE, 'MMM D YYYY'),
          rid: el.Reserves_category + ' ' + el.REQUESTID
        }
      ))
      state.bHistory.ft.forEach(el => state.bHistory.shapes.geoJSONs.push(el.geojson))
    }
  },

  RESET_BLOCKSHAPE (state) {
    state.bHistory.block.shape = null
  },

  SET_BLOCKSHAPE (state) { // нет payload и работает
    if (state.bHistory.ft) {
      state.bHistory.block.shape = state.bHistory.ft[state.bHistory.ft.length - 1].geojson
    }
  }

}

// actions
const actions = {

  async clearBHistory (context) {
    await context.commit('RESET_BHISTORYFT')
    await context.commit('RESET_BHSHAPES')
    await context.commit('RESET_BLOCKSHAPE')
  },

  async setBHistory (context, payload) {
    let url = ''
    if (payload[1] === 'TB_DEM') {
      url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/bh/tblockgeojson.aspx?tblock=' + payload[0]
    } else if (payload[1] === 'TB_GEOS') {
      url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'BH_TBLOCK_GEOS_GEOJSON' + '&p1=' + payload[0] + '&orderby=' + 'GDB_FROM_DATE'
    } else if (payload[1] === '3D_GEOS') {
      url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'BH_3D_GEOJSON' + '&p1=' + payload[0]
    } else if (payload[1] === 'DEPO') {
      url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/bh/tblockgeojson.aspx?tblock=' + payload[0] + '&type=' + payload[1]
    }
    await f.fget(url)
      .then(res => {
        context.commit('SET_BHISTORYFT', res.content)
      })
    context.commit('SET_BHSHAPES')
    context.commit('SET_BLOCKSHAPE')
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
