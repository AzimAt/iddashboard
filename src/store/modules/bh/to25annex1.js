// BEGIN importing axios and set settings because we cannot use plugin in store
import axios from 'axios'
const ax = axios.create({ timeout: 60000, withCredentials: true, headers: { Accept: 'application/json', 'Content-Type': 'application/json' } })
// END of importing axios and set settings because we cannot use plugin in store

const state = () => ({

  to25blocks: {
    ft: null // full table
  }
})

// getters
const getters = {
  getTO25BlocksFT: (state) => {
    return state.to25blocks.ft
  }
}
// mutations
const mutations = {
  SET_TO25BLOCKSFT (state, payload) {
    state.to25blocks.ft = payload
    // console.log(state.to25blocks.ft)
  },

  RESET_TO25BLOCKSFT (state) {
    state.to25blocks.ft = null
  }

}

// actions
const actions = {
  async setTO25Blocks (context, tblock) {
    context.commit('RESET_TO25BLOCKSFT')
    const res = await ax.get(
      'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'TO25_ANNEX_1' + '&p1=' + tblock)
    context.commit('SET_TO25BLOCKSFT', res.data.content)
  }

}

export default {

  namespaced: true,

  state,

  getters,

  actions,

  mutations

}
