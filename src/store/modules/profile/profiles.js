import f from '@/store/fetch.js'

const state = () => ({
  profiles: {
    ft: null // full table
  }
})

const getters = {
  getProfiles: (state) => {
    return state.profiles.ft
  }
}
const mutations = {
  SET_PROFILES (state, payload) {
    state.profiles.ft = payload
  },
  RESET_PROFILES (state) {
    state.profiles.ft = null
  }
}

const actions = {
  async setProfiles (context, payload) {
    context.commit('RESET_PROFILES')
    const url = 'https://gis-katco.bdom.ad.corp/asp/geos/idcard/querytfn1.aspx?tfn=' + 'PROFILES' + '&orderby=name'
    f.fget(url)
      .then(res => {
        // console.log('h4. setProfiles started.')
        context.commit('SET_PROFILES', res.content)
      })
  },
  resetProfiles (context) {
    context.commit('RESET_PROFILES')
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
