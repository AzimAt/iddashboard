import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'

import './assets/main.css'


import { createApp } from 'vue'
import { createStore } from 'vuex'



// Create a new store instance.
import ui from './store/modules/ui/ui'
import ui2 from './store/modules/ui/ui2'
import ui3 from './store/modules/ui/ui3'
import geos3d from './store/modules/ui/geos3d'
import profiles from './store/modules/profile/profiles'

import drillholes from './store/modules/wells/drillholes'
import sde from './store/modules/blocks/sde'
import tblocks from './store/modules/blocks/tblocks'
import tblocksgeos from './store/modules/blocks/tblocksgeos'
import deposits from './store/modules/deposit/deposits'
import dhs from './store/modules/ui/dhs'

import wellsgt from './store/modules/wells/wellsgt'
import wells from './store/modules/wells/wells'
import wellshsgt from './store/modules/wells/wellshsgt'
import wellspiacq from './store/modules/wells/wellspiacq'
import wellsgtmap from './store/modules/wells/wellsgtmap'
import wellsgrm from './store/modules/wells/wellsgrm'
import injectingproducers from './store/modules/blocks/injectingproducers'
import sdecells from './store/modules/blocks/sdecells'
import dhsummary1 from './store/modules/wells/dhsummary1'
import dhsummary2 from './store/modules/wells/dhsummary2'
import dhsummary3 from './store/modules/wells/dhsummary3'
import depositparam from './store/modules/blocks/depositparam'
import bhistory from './store/modules/bh/bhistory'
import to25annex1 from './store/modules/bh/to25annex1'
import holedetails from './store/modules/wells/holedetails'
import qcresultsbydeposit from './store/modules/deposit/qcresultsbydeposit'
import depositsamples from './store/modules/deposit/depositsamples'
import dh3dhistory from './store/modules/wells/dh3dhistory'
import holexrf from './store/modules/wells/holexrf'

import holesamples from './store/modules/wells/holesamples'
import holesamplesgra from './store/modules/wells/holesamplesgra'
import holesamplesco from './store/modules/wells/holesamplesco'

import {version} from '../package'
// console.log('version=', version)

const store = createStore({
  state () {
    return {
      count: 0,
      AppVersion: version
    }
  },
  mutations: {
    increment (state) {
      state.count++
    }
  },
  modules: {
    ui,
    ui2,
    ui3,
    tblocks,
    tblocksgeos,
    geos3d,
    deposits,
    profiles,
    dhs,
    drillholes,
    sde,
    // techBlock
    wellsgt,
    wells,
    wellshsgt,
    wellspiacq,
    wellsgtmap,
    wellsgrm,
    injectingproducers,
    sdecells,
    dhsummary1,
    dhsummary2,
    dhsummary3,
    depositparam,
    bhistory,
    to25annex1,
    holedetails,
    qcresultsbydeposit,
    depositsamples,
    dh3dhistory,
    holexrf,
    holesamples,
    holesamplesgra,
    holesamplesco
  }
})


import HelloWorld from './components/HelloWorld.vue'
import TheWelcome from './components/TheWelcome.vue'


/* const routes = [{ path: '/', component: 
    HomePage },] */
// moved to  @src/router/index.js

import routesV from "./router";

import { createRouter, createWebHashHistory } from 'vue-router'
const router = createRouter({
  history: createWebHashHistory(),
  routes: routesV
})

import AboutPage from './views/sandbox/AboutPage.vue'
router.addRoute({ path: '/about', name: 'aboutPage', component: AboutPage })

// Vuetify
import 'vuetify/styles'
import '@mdi/font/css/materialdesignicons.css'

import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
const vuetify = createVuetify({
  components,
  directives,
  ssr: true,
  theme: {
    defaultTheme: 'light'
  }
})

// PrimeVue (UI Suite for Vue.js)
//import 'primevue/resources/themes/aura-light-green/theme.css'
//import "primeflex/primeflex.css";
//import "primevue/resources/themes/lara-light-green/theme.css";
//import "primevue/resources/primevue.min.css"; /* Deprecated */
//import "primeicons/primeicons.css";

/* import PrimeVue from 'primevue/config';
import DataTable from 'primevue/datatable';
import Column from 'primevue/column';
import ColumnGroup from 'primevue/columngroup';   // optional
import Row from 'primevue/row';                   // optional */

// custom component imporing list
import App from './App.vue'
import AppBar from '@/../src/components/ui/AppBar.vue'
import AppFooter from './components/ui/AppFooter.vue'
import KHelpDialog from './components/ui/KHelpDialog.vue'
import App3dGEOSAutoComplete from './components/ui/App3dGEOSAutoComplete.vue'
import AppDrawerMenu from './components/ui/AppDrawerMenu.vue'
import AppProfileAutoComplete from './components/ui/AppProfileAutoComplete.vue'
import AppBlockAutoComplete from './components/ui/AppBlockAutoComplete.vue'
import AppDepositAutoComplete from './components/ui/AppDepositAutoComplete.vue'
import AppDHDataAutoComplete from './components/ui/AppDHDataAutoComplete.vue'
import AppDHsAutoComplete from './components/ui/AppDHsAutoComplete.vue'
import AppGEOSTBlockAutoComplete from './components/ui/AppGEOSTBlockAutoComplete.vue'
import AppReportAutoComplete from './components/ui/AppReportAutoComplete.vue'
import AppSamplingAutoComplete from './components/ui/AppSamplingAutoComplete.vue'
import DrillholesDT from './components/id/DrillholesDT.vue'
import DHDataTable from './components/dh/DHDataTable.vue'
import Loading from './components/ui/Loading.vue'

import DataTableHeaderColumn from './components/ui/DataTableHeaderColumn.vue'
import HoleID from './components/ui/HoleID.vue'
import ShapeInfo from './components/ui/ShapeInfo.vue'
import HoleStatus from './components/dh/HoleStatus.vue'
import XRFStatus from './components/ui/XRFStatus.vue'
import GikletStatus from './components/dh/GikletStatus.vue'
import JsonExcel from 'vue-json-excel3'

import SdeTblockDT from './components/id/SdeTblockDT.vue'
import DepositParamDT from './components/id/DepositParamDT.vue'
import DepositShortInfo from './components/id/DepositShortInfo.vue'
import RCMapSelector from './components/rc/RCMapSelector.vue'

// added by YS on 2021-12-24 for https://dev.azure.com/orano/DevOps%20BU%20Mines/_backlogs/backlog/idcard/Features/?workitem=29071
import RCMapSelector2 from './components/rc/RCMapSelector2.vue'
import SvgCircle from './components/svg/SvgCircle.vue'
import WellCircleMarkers from './components/gis/WellCircleMarkers.vue' // this one is not used now
import Drillholes3DT from './components/id/Drillholes3DT.vue'
import HoleDT from './components/id/HoleDT.vue'
import DHTabs from './components/ui/DHTabs.vue'

import AdminRightsDT from './components/ad/AdminRightsDT.vue'
import UserList from './components/ad/UserList.vue'
import GroupsList from './components/ad/GroupsList.vue'
import RoutesOfGroup from './components/ad/RoutesOfGroup.vue'

import DialogComponent from './components/ui/DialogComponent.vue'
import QCResultsDataTableVe from './components/qc/QCResultsDataTableVe.vue'
import QCResultsDataTableVeWellsList from './components/qc/QCResultsDataTableVeWellsList.vue'

import DepositSamplesDT from './components/id/DepositSamplesDT.vue'

import DepositSamplesDTGikletDone from './components/id/DepositSamplesDTGikletDone.vue'
import DepositSamplesDTLogginTypeDem from './components/id/DepositSamplesDTLogginTypeDem.vue'
import DepositSamplesDTDemGk from './components/id/DepositSamplesDTDemGk.vue'
import DepositSamplesDTLogginTypes from './components/id/DepositSamplesDTLogginTypes.vue'

import DepositSamplesDTDemGkWellsList from './components/id/DepositSamplesDTDemGkWellsList.vue'
import DepositSampleTableItemMenu from './components/id/DepositSampleTableItemMenu.vue'
import DepositSamplesDTLogginTypesWellsList from './components/id/DepositSamplesDTLogginTypesWellsList.vue'
import DepositSamplesDTGikletDoneWellsList from './components/id/DepositSamplesDTGikletDoneWellsList.vue'

import RCMapByDeposit from './components/rc/RCMapByDeposit.vue'

import DHSummary1ST from './components/dh/DHSummary1ST.vue'
import DHSummary2ST from './components/dh/DHSummary2ST.vue'
import DHSummary3ST from './components/dh/DHSummary3ST.vue'

import BHTO25Annex1DT from './components/bh/BHTO25Annex1DT.vue'
import BHISRTechBlocksHDT from './components/bh/BHISRTechBlocksHDT.vue'
import BHShape from './components/bh/BHShape.vue'
import ColorPicker from './components/ui/ColorPicker.vue'

import RCGisSelectDataTable from './components/rc/RCGisSelectDataTable.vue'
import RCGtDataTable from './components/rc/RCGtDataTable.vue'



// Leaflet
import "leaflet/dist/leaflet.css"
import {LMap, LTileLayer, LMarker, LCircleMarker, LLayerGroup, LFeatureGroup, LTooltip, LGeoJson, LPopup, LIcon } from "@vue-leaflet/vue-leaflet"


// const app = createApp(App).use(store).use(router).use(vuetify).mount('#app')
const app = createApp(App)


// registration custom components 
app.component('AppBar', AppBar)
app.component('AppFooter', AppFooter)
app.component('KHelpDialog', KHelpDialog)
app.component('App3dGEOSAutoComplete', App3dGEOSAutoComplete)
app.component('AppDrawerMenu', AppDrawerMenu)
app.component('AppProfileAutoComplete', AppProfileAutoComplete)
app.component('AppBlockAutoComplete', AppBlockAutoComplete)
app.component('AppDepositAutoComplete', AppDepositAutoComplete)
app.component('AppDHDataAutoComplete', AppDHDataAutoComplete)
app.component('AppDHsAutoComplete', AppDHsAutoComplete)
app.component('AppGEOSTBlockAutoComplete', AppGEOSTBlockAutoComplete)
app.component('AppReportAutoComplete', AppReportAutoComplete)
app.component('AppSamplingAutoComplete', AppSamplingAutoComplete)
app.component('DrillholesDT', DrillholesDT)
app.component('DHDataTable', DHDataTable)
app.component('Loading', Loading)

app.component('DataTableHeaderColumn', DataTableHeaderColumn)
app.component('HoleID', HoleID)
app.component('ShapeInfo', ShapeInfo)
app.component('HoleStatus', HoleStatus)
app.component('XRFStatus', XRFStatus)
app.component('GikletStatus', GikletStatus)
app.component('downloadExcel', JsonExcel)
app.component('SdeTblockDT', SdeTblockDT)
app.component('DepositParamDT', DepositParamDT)
app.component('DepositShortInfo', DepositShortInfo)
app.component('RCMapSelector', RCMapSelector)
app.component('RCMapSelector2', RCMapSelector2)
app.component('SvgCircle', SvgCircle)
app.component('WellCircleMarkers', WellCircleMarkers)
app.component('Drillholes3DT', Drillholes3DT)
app.component('HoleDT', HoleDT)
app.component('DHTabs', DHTabs)

app.component('AdminRightsDT', AdminRightsDT)
app.component('UserList', UserList)
app.component('GroupsList', GroupsList)
app.component('RoutesOfGroup', RoutesOfGroup)
app.component('DialogComponent', DialogComponent)
app.component('QCResultsDataTableVe', QCResultsDataTableVe)
app.component('QCResultsDataTableVeWellsList', QCResultsDataTableVeWellsList)

app.component('DepositSamplesDT', DepositSamplesDT)
app.component('DepositSamplesDTLogginTypes', DepositSamplesDTLogginTypes)
app.component('DepositSamplesDTGikletDone', DepositSamplesDTGikletDone)
app.component('DepositSamplesDTLogginTypeDem', DepositSamplesDTLogginTypeDem)
app.component('DepositSamplesDTDemGk', DepositSamplesDTDemGk)

app.component('DepositSamplesDTDemGkWellsList', DepositSamplesDTDemGkWellsList)
app.component('DepositSampleTableItemMenu', DepositSampleTableItemMenu)
app.component('DepositSamplesDTLogginTypesWellsList', DepositSamplesDTLogginTypesWellsList)
app.component('DepositSamplesDTGikletDoneWellsList', DepositSamplesDTGikletDoneWellsList)
app.component('RCMapByDeposit', RCMapByDeposit)
app.component('DHSummary1ST', DHSummary1ST)
app.component('DHSummary2ST', DHSummary2ST)
app.component('DHSummary3ST', DHSummary3ST)

app.component('BHTO25Annex1DT', BHTO25Annex1DT)
app.component('BHISRTechBlocksHDT', BHISRTechBlocksHDT)
app.component('BHShape', BHShape)
app.component('ColorPicker', ColorPicker)

app.component('RCGisSelectDataTable', RCGisSelectDataTable)
app.component('RCGtDataTable', RCGtDataTable)

import Scatter from './components/ui/Scatter.vue'
app.component('Scatter', Scatter)

import GTStat from './components/rc/GTStat.vue'
app.component('GTStat', GTStat)

import VChart from 'vue-echarts';
app.component('EVChart', VChart)

import RCHsGtDataTable from './components/rc/RCHsGtDataTable.vue'
app.component('RCHsGtDataTable', RCHsGtDataTable)

import RCGrmDataTable from './components/rc/RCGrmDataTable.vue'
app.component('RCGrmDataTable', RCGrmDataTable)

import QCOsidem1 from './components/qc/QCOsidem1.vue'
app.component('QCOsidem1', QCOsidem1)

import RCMapSelectorISR1 from './components/rc/RCMapSelectorISR1.vue'
app.component('RCMapSelectorISR1', RCMapSelectorISR1)

import KDataTable from './components/ui/KDataTable.vue'
app.component('KDataTable', KDataTable)

import DHSummary4ST from './components/dh/DHSummary4ST.vue'
app.component('DHSummary4ST', DHSummary4ST)


import DH3DhistoryDT from './components/id/DH3DhistoryDT.vue'
app.component('DH3DhistoryDT', DH3DhistoryDT)

import DH3DhistDataTable from './components/dh/DH3DhistDataTable.vue'
app.component('DH3DhistDataTable', DH3DhistDataTable)

import HoleXRF from './components/dh/HoleXRF.vue'
app.component('HoleXRF', HoleXRF)



import DHLogColumn from './components/log/DHLogColumn.vue'
app.component('DHLogColumn', DHLogColumn)
import LogSectionScale from './components/log/LogSectionScale.vue'
app.component('LogSectionScale', LogSectionScale)

import LogSection from './components/log/LogSection.vue'
app.component('LogSection', LogSection)

import LogYScale from './components/log/LogYScale.vue'
app.component('LogYScale', LogYScale)

import LogHistogram from './components/log/LogHistogram.vue'
app.component('LogHistogram', LogHistogram)

import LogLine from './components/log/LogLine.vue'
app.component('LogLine', LogLine)

import LogGrid from './components/log/LogGrid.vue'
app.component('LogGrid', LogGrid)

import HoleSamples from './components/dh/HoleSamples.vue'
app.component('HoleSamples', HoleSamples)

import HoleSamplesCo from './components/dh/HoleSamplesCo.vue'
app.component('HoleSamplesCo', HoleSamplesCo)

import HoleSamplesGra from './components/dh/HoleSamplesGra.vue'
app.component('HoleSamplesGra', HoleSamplesGra)

import AccessDeniedPage from './views/AccessDeniedPage.vue'
app.component('AccessDeniedPage', AccessDeniedPage)

import DemDashboardleWellsList from './components/dh/DemDashboardleWellsList.vue'
app.component('DemDashboardleWellsList', DemDashboardleWellsList)


app.component('l-map', LMap)
app.component('l-tile-layer', LTileLayer)
app.component('l-marker', LMarker)
app.component('l-icon', LIcon)
app.component('l-circle-marker', LCircleMarker)
app.component('l-layer-group', LLayerGroup)
app.component('l-feature-group', LFeatureGroup)
app.component('l-tooltip', LTooltip)
app.component('l-geo-json', LGeoJson)
app.component('l-popup', LPopup) 



// PrimeVue components
/* app.component('pv-DataTable', DataTable)
app.component('pv-Column', Column)
app.component('pv-ColumnGroup', ColumnGroup)
app.component('pv-Row', Row) */


// registration plugins
app.use(store)
app.use(router)
app.use(vuetify)
// app.use(PrimeVue);

app.mount('#app')

