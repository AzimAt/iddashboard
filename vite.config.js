import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import basicSsl from '@vitejs/plugin-basic-ssl'

console.log(' @ = ', fileURLToPath(new URL('./src', import.meta.url)))
// https://vitejs.dev/config/
export default defineConfig({
  server: {
    port: 4343
  },
  base: './',
  plugins: [
    vue(),
    basicSsl(),
    
  ],
  resolve: {
    extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue'],
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  build: {
    target: 'esnext' //browsers can handle the latest ES features
  }
})
